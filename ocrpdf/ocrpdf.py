import pytesseract
import PyMuPDF


text = pytesseract.image_to_pdf_or_hocr(image, extension='pdf')











# import fitz  # PyMuPDF
# import pytesseract
# from pdf2image import convert_from_path
# from pytesseract import Output
# from io import BytesIO
# from PIL import Image

# def ocr_to_searchable_pdf(input_pdf_path, output_pdf_path):
#     # Open the existing PDF
#     pdf_document = fitz.open(input_pdf_path)

#     # List to store images with OCR text layer
#     ocr_pages = []

#     # Iterate through PDF pages
#     for page_number in range(len(pdf_document)):
#         # Convert page to an image
#         images = convert_from_path(input_pdf_path, first_page=page_number + 1, last_page=page_number + 1)
#         for image in images:
#             # Perform OCR on the image
#             text = pytesseract.image_to_pdf_or_hocr(image, extension='pdf')

#             # Open the OCR result as a PDF page
#             ocr_pdf = fitz.open("pdf", text)

#             # Combine the original page with OCR text layer
#             page = pdf_document.load_page(page_number)
#             rect = page.rect
#             ocr_page = ocr_pdf[0]
#             ocr_page.show_pdf_page(rect, pdf_document, page_number)
#             ocr_pages.append(ocr_page)

#     # Create a new PDF with OCR layers
#     ocr_document = fitz.open()
#     for ocr_page in ocr_pages:
#         ocr_document.insert_pdf(ocr_page)

#     # Save the new searchable PDF
#     ocr_document.save(output_pdf_path)
#     ocr_document.close()

# # Usage
# input_pdf_path = 'input.pdf'
# output_pdf_path = 'output_searchable.pdf'
# ocr_to_searchable_pdf(input_pdf_path, output_pdf_path)
