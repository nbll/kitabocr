# minimal viable product
# as fast as possible
# priority VERY HIIIIGH

# TODO: read file from filesstem

# with open("/home/nabil/cs/kitabocr/files1.txt", "r") as file:
#     file_data = file.read()
#     print(file_data)

import PyPDF2

with open("example.pdf", "rb") as pdf_file:
    pdf_reader = PyPDF2.PdfFileReader(pdf_file)
    print(pdf_reader.getDocumentInfo())
    print(pdf_reader.getNumPages())  # Get the number of pages
    for page_num in range(pdf_reader.getNumPages()):
        page = pdf_reader.getPage(page_num)
        print(page.extractText())  # Extract text from each page
