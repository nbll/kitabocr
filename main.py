#!/usr/bin/env python

import sys # needed form access to command line arguments

from PySide6.QtCore import QSize, Qt
from PySide6.QtWidgets import QApplication, QMainWindow, QPushButton




# Subclass QMainWindow to customize your app's main window
class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("My first app")

        button = QPushButton("Press the buttoooon")

        # set the central widget of the Window
        self.setCentralWidget(button)

# Subclass QMainWindow to customize your app's main window
class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("My first app")

        button = QPushButton("Press the buttoooon")

        # set the central widget of the Window
        self.setCentralWidget(button)

# ONLY ONE QApplication instance per application
app = QApplication(sys.argv)

# window = QWidget() # the qt widget will be our window
# window = QPushButton("Push me!")
window = MainWindow()
window.show() # Windows are hidden by default

app.exec() # start the event loop
